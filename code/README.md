## Reuqirements: 

- clingo installed in Path (https://github.com/potassco/clingo)
- runsolver (https://www.cril.univ-artois.fr/~roussel/runsolver/) installed in Path
- SystemC installed (findable by CMake, https://github.com/accellera-official/systemc/tree/release)
- at least C\+\+-14 compatible compiler (C\+\+-17 recommended, GCC8 and GCC9 are tested, others may or may not work)

## Build:

#### Build full_ass (i.e., full assignment)
- cmake .. -DCLINGO_HOME="path to CLINGO"
- cmake --build . -config=Release

### SystemC Simulation
- Closed Source application
- please contact the authors
- kai.neubauer at uni-rostock.de
- benjamin.beichler at uni-rostock.de
- christian.haubelt at uni-rostock.de

## Run:

#### Approximation Benchmarks:

- ./benchmark.sh

#### Exact Benchmarks:
- split into three scripts 
- ./benchmarks_exact_only_?.sh

#### Full Coverage Benchmark:
- benchmark_small_\*.sh