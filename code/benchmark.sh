#!/bin/bash

timelimit=3600
killdelay=10
threads=8
full_ass="./full_ass/theory ./preferences/theory/preference_front.lp"
encodingAPPROX="./encodings/encoding_hop_arb_approx_storeforeward.lp"
instances="./instances/benchmarks2019"
results=${instances}"/results"

for filename in ${instances}/*.lp; do
	echo $filename
	for method in "$full_ass"; do
		for type in $encodingAPPROX; do
			timestamp=$( date +%Y%m%d_%H%M%S)
			result_filename=$(basename "$filename" .lp)_mult001_${timestamp}.impl
			echo $result_filename
			runsolver --use-pty --timestamp -W $timelimit -d $killdelay -o ${results}/${result_filename} $method ./encodings/dl_lang.lp $type ./encodings/priorities_approx.lp $filename -n=0 -v -p -a -- -t${threads} -c mult=1 --config=./configs/t5.txt 
			sleep 1s
			timestamp=$( date +%Y%m%d_%H%M%S)
			result_filename=$(basename "$filename" .lp)_mult010_${timestamp}.impl
			echo $result_filename
			runsolver --use-pty --timestamp -W $timelimit -d $killdelay -o ${results}/${result_filename} $method ./encodings/dl_lang.lp $type ./encodings/priorities_approx.lp $filename -n=0 -v -p -a -- -t${threads} -c mult=10 --config=./configs/t5.txt
			sleep 1s
			timestamp=$( date +%Y%m%d_%H%M%S)
			result_filename=$(basename "$filename" .lp)_mult100_${timestamp}.impl
			echo $result_filename
			runsolver --use-pty --timestamp -W $timelimit -d $killdelay -o ${results}/${result_filename} $method ./encodings/dl_lang.lp $type ./encodings/priorities_approx.lp $filename -n=0 -v -p -a -- -t${threads} -c mult=100 --config=./configs/t5.txt
			sleep 1s
		done
	done
done

