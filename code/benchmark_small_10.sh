#!/bin/bash

touch /dev/shm/noshutdown

killdelay=10

full_ass="./full_ass/theory ./preferences/theory/preference_front.lp"

instances="./instances/benchmarks2019"
results=${instances}"/full_coverage/results"

#Small Testcase with Approximation -- no time limit
filename=$instances/full_coverage/medium_test3.lp
echo $filename
timestamp=$( date +%Y%m%d_%H%M%S)
result_filename=$(basename "$filename" .lp)_approx010_${timestamp}.impl
echo $result_filename
runsolver --use-pty --timestamp -d $killdelay -o $results/${result_filename} $full_ass ./encodings/dl_lang.lp ./encodings/priorities_approx.lp $filename ./encodings/encoding_xyz_approx.lp -n=0 -v -p -a -- -c mult=10

sleep 1s

#Small Testcase without Approximation -- no time limit
timestamp=$( date +%Y%m%d_%H%M%S)
result_filename=$(basename "$filename" .lp)_exactonly010_${timestamp}.impl
echo $result_filename
runsolver --use-pty --timestamp -d $killdelay -o $results/${result_filename} $full_ass ./encodings/dl_lang.lp ./encodings/priorities_approx.lp $filename ./encodings/encoding_xyz_approx.lp -n=0 -v -p -- -c mult=10

sleep 1s
