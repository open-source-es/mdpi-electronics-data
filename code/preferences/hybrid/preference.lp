#const theories = (idl).

#program specification.
_optimize(all).

_preference(all,pareto).
_preference(all,(1,1,()),1,name(p1),()).
_preference(all,(1,2,()),1,name(p2),()).
_preference(all,(1,3,()),1,name(p3),()).

_preference(p1,latency).
_preference(p1,(1,1,(M,T,A,R,Time)),1,for(atom(bind(M,task(T,A),R))),(Time,task(T,A),bind(M,task(T,A)))) :- map(M,task(T,A),R), executionTime(M,Time).
_holds(atom(bind(M,task(T,A),R)),0) :- bind(M,task(T,A),R), map(M,task(T,A),R), executionTime(M,Time).
_preference(p1,(1,1,()),1,depends(idl),()).

_preference(p2,less(weight)).
_preference(p2,(2,1,(A,P,R,M,T)),1,for(atom(bind(M,task(T,A),R))),(P,bind(M,task(T,A),R))) :- map(M,task(T,A),R), dynamicEnergy(M,P).
_holds(atom(bind(M,task(T,A),R)),0) :- bind(M,task(T,A),R), map(M,task(T,A),R), dynamicEnergy(M,P).
_preference(p2,(2,2,(P,R)),1,for(atom(allocated(R))),(P,allocated(R))) :- staticPower(R,P).
_holds(atom(allocated(R)),0) :- allocated(R), staticPower(R,P).
_preference(p2,(2,3,(P,R1,R2)),1,for(atom(reached(comm(T1,T2,A),R1,R2))),(P,reached(comm(T1,T2,A),R1,R2))) :- link(R1,R2), comm(T1,T2,A), routingEnergy(P).
_holds(atom(reached(comm(T1,T2,A),R1,R2)),0) :- reached(comm(T1,T2,A),R1,R2), link(R1,R2), comm(T1,T2,A), routingEnergy(P).

_preference(p3,less(weight)).
_preference(p3,(3,1,(C,R)),1,for(atom(allocated(R))),(C,allocated(R))) :- resourceCost(R,C).
_holds(atom(allocated(R)),0) :- allocated(R), resourceCost(R,C).

#program preference_base.
__required(P,better) :- _optimize(P).
__required(Q,better) :- _preference(P,pareto); __required(P,better); _preference(P,_,_,name(Q),_).
__required(Q,bettereq) :- _preference(P,pareto); __required(P,better); _preference(P,_,_,name(Q),_).
__required(Q,eq) :- _preference(P,pareto); __required(P,eq); _preference(P,_,_,name(Q),_).
__required(Q,worseeq) :- _preference(P,pareto); __required(P,worseeq); _preference(P,_,_,name(Q),_).
__required(Q,worse) :- _preference(P,pareto); __required(P,worse); _preference(P,_,_,name(Q),_).
__required(Q,worseeq) :- _preference(P,pareto); __required(P,worse); _preference(P,_,_,name(Q),_).
__required(P,bettereq) :- _preference(P,pareto); __required(P,unc).
__required(P,worseeq) :- _preference(P,pareto); __required(P,unc).

#program preference(_m1,_m2).
_unsat(_m(_m1),_m(_m2)) :- not __better(P,_m(_m1),_m(_m2)); _optimize(P); _volatile(_m(_m1),_m(_m2)).

__better(P,_m(_m1),_m(_m2)) :- _preference(P,less(weight)); __required(P,better); 1 <= #sum { -W,T : _holds(X,_m1), _preference(P,_,_,for(X),T), W=@get(T,0), _volatile(_m(_m1),_m(_m2)); W,T : _holds(X,_m2), _preference(P,_,_,for(X),T), W=@get(T,0), _volatile(_m(_m1),_m(_m2)) }; _volatile(_m(_m1),_m(_m2)).
__bettereq(P,_m(_m1),_m(_m2)) :- _preference(P,less(weight)); __required(P,bettereq); 0 <= #sum { -W,T : _holds(X,_m1), _preference(P,_,_,for(X),T), W=@get(T,0), _volatile(_m(_m1),_m(_m2)); W,T : _holds(X,_m2), _preference(P,_,_,for(X),T), W=@get(T,0), _volatile(_m(_m1),_m(_m2)) }; _volatile(_m(_m1),_m(_m2)).
__eq(P,_m(_m1),_m(_m2)) :- _preference(P,less(weight)); __required(P,eq); 0 <= #sum { -W,T : _holds(X,_m1), _preference(P,_,_,for(X),T), W=@get(T,0), _volatile(_m(_m1),_m(_m2)); W,T : _holds(X,_m2), _preference(P,_,_,for(X),T), W=@get(T,0), _volatile(_m(_m1),_m(_m2)) } <= 0; _volatile(_m(_m1),_m(_m2)).
__worseeq(P,_m(_m1),_m(_m2)) :- _preference(P,less(weight)); __required(P,worseeq); 0 >= #sum { -W,T : _holds(X,_m1), _preference(P,_,_,for(X),T), W=@get(T,0), _volatile(_m(_m1),_m(_m2)); W,T : _holds(X,_m2), _preference(P,_,_,for(X),T), W=@get(T,0), _volatile(_m(_m1),_m(_m2)) }; _volatile(_m(_m1),_m(_m2)).
__worse(P,_m(_m1),_m(_m2)) :- _preference(P,less(weight)); __required(P,worse); -1 >= #sum { -W,T : _holds(X,_m1), _preference(P,_,_,for(X),T), W=@get(T,0), _volatile(_m(_m1),_m(_m2)); W,T : _holds(X,_m2), _preference(P,_,_,for(X),T), W=@get(T,0), _volatile(_m(_m1),_m(_m2)) }; _volatile(_m(_m1),_m(_m2)).

__better(P,_m(_m1),_m(_m2)) :- _preference(P,pareto); __required(P,better); __bettereq(Q,_m(_m1),_m(_m2)) : _preference(P,_,_,name(Q),_); __better(R,_m(_m1),_m(_m2)); _preference(P,_,_,name(R),_); _volatile(_m(_m1),_m(_m2)).
__bettereq(P,_m(_m1),_m(_m2)) :- _preference(P,pareto); __required(P,bettereq); __bettereq(Q,_m(_m1),_m(_m2)) : _preference(P,_,_,name(Q),_); _volatile(_m(_m1),_m(_m2)).
__eq(P,_m(_m1),_m(_m2)) :- _preference(P,pareto); __required(P,eq); __eq(Q,_m(_m1),_m(_m2)) : _preference(P,_,_,name(Q),_); _volatile(_m(_m1),_m(_m2)).
__worseeq(P,_m(_m1),_m(_m2)) :- _preference(P,pareto); __required(P,worseeq); __worseeq(Q,_m(_m1),_m(_m2)) : _preference(P,_,_,name(Q),_); _volatile(_m(_m1),_m(_m2)).
__worse(P,_m(_m1),_m(_m2)) :- _preference(P,pareto); __required(P,worse); __worseeq(Q,_m(_m1),_m(_m2)) : _preference(P,_,_,name(Q),_); __worse(R,_m(_m1),_m(_m2)); _preference(P,_,_,name(R),_); _volatile(_m(_m1),_m(_m2)).
__unc(P,_m(_m1),_m(_m2)) :- _preference(P,pareto); __required(P,unc); not __bettereq(P,_m(_m1),_m(_m2)); not __worseeq(P,_m(_m1),_m(_m2)); _volatile(_m(_m1),_m(_m2)).

