/*
	{{{ MIT License

	Copyright 2020 Kai Neubauer <kai.neubauer at uni-rostock.de> and Philipp Wanko <wanko at cs.uni-potsdam.de>

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
	associated documentation files (the "Software"), to deal in the Software without restriction, 
	including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
	and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
	subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial 
	portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
	LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
	IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE

	}}}
*/
#ifndef LIBPREF_H_
#define LIBPREF_H_

#include <clingo.hh>
#include <iostream>
#include <string>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <limits>
#include <chrono>
#include <iomanip>
#include <stdlib.h>
#include "libtheory.h"
#include <memory>

using namespace Clingo;
class Preference: public Propagator {
public:
	Preference(std::string name) {
		this->name = name;
	}

	virtual void print_quality(unsigned int model)=0;
	virtual void save_model(unsigned int thread, unsigned int model)=0;
	virtual bool better(unsigned int thread, unsigned int model)=0;
	virtual bool bettereq(unsigned int thread, unsigned int model)=0;
	virtual bool worse(unsigned int thread, unsigned int model)=0;
	virtual std::vector<literal_t> reason(PropagateControl &ctl)=0;
	virtual std::vector<int> current_best()=0;
	virtual void print_stats(int thread)=0;
	virtual void print_stats()=0;
	virtual void set_config(char *argv[], int argc, Control &ctl)=0;

	virtual bool calc_approx(PropagateControl &ctl) = 0;
	virtual bool calc_exact(PropagateControl &ctl) = 0;
	virtual void remove_exact(Clingo::id_t thread) = 0;
	virtual void remove_approx(Clingo::id_t thread) = 0;

	virtual bool is_empty(Clingo::id_t thread) = 0;

	virtual void optimum_found() {
		return;
	}

	bool is_composite() {
		return composite;
	}

	std::string get_name() const {
		return name;
	}

	void add_subpref(std::shared_ptr<Preference> pref) {
		subprefs.emplace_back(pref);
	}

	std::vector<std::shared_ptr<Preference>>& get_subprefs() {
		return subprefs;
	}

private:
	std::string name;
	bool composite;
	std::vector<std::shared_ptr<Preference>> subprefs;
};

class PreferenceCreator {
public:
	virtual std::shared_ptr<Preference> create(std::string name)=0;
};

class PreferenceFactory {
public:
	static void register_preference(std::string type,
			std::shared_ptr<PreferenceCreator> preference_creator);
	static std::shared_ptr<Preference> get_preference(std::string type,
			std::string name);

private:

	static std::unordered_map<std::string, std::shared_ptr<PreferenceCreator>> &getCreators() {
		static std::unordered_map<std::string,
				std::shared_ptr<PreferenceCreator>> creators;
		return creators;
	}

	static std::map<std::pair<std::string, std::string>,
			std::shared_ptr<Preference>> &getPreferences() {
		static std::map<std::pair<std::string, std::string>,
				std::shared_ptr<Preference>> preferences;
		return preferences;
	}

//    static std::unordered_map<std::string,std::shared_ptr<PreferenceCreator>> creators;
//    static std::map<std::pair<std::string,std::string>,std::shared_ptr<Preference>> preferences;
};

struct PreferenceNode {
	PreferenceNode() {
	}
	PreferenceNode(std::string name, std::string type) :
			name(name), type(type) {
	}
	std::vector<std::string> preference_dependencies;
	std::string name;
	std::string type;
	std::vector<std::string> theory_dependencies;
};

class PreferenceGraph {
public:
	PreferenceGraph(Control &ctl) {
		for (SymbolicAtomIterator it = ctl.symbolic_atoms().begin(
				Signature("_preference", 2)); it != ctl.symbolic_atoms().end();
				it++) {
			std::string name = it->symbol().arguments()[0].to_string();
			std::string type = it->symbol().arguments()[1].to_string();
			nodes[name] = PreferenceNode(name, type);
		}
		for (SymbolicAtomIterator it = ctl.symbolic_atoms().begin(
				Signature("_preference", 5)); it != ctl.symbolic_atoms().end();
				it++) {
			std::string name = it->symbol().arguments()[0].to_string();
			if (it->symbol().arguments()[3].type() != SymbolType::Function) {
				continue;
			}
			std::string attrib_type = it->symbol().arguments()[3].name();
			std::string attrib_value =
					it->symbol().arguments()[3].arguments()[0].to_string();
			if (attrib_type == "name") {
				nodes[name].preference_dependencies.emplace_back(attrib_value);
			} else if (attrib_type == "depends") {
				nodes[name].theory_dependencies.emplace_back(attrib_value);
			}
		}
		for (SymbolicAtomIterator it = ctl.symbolic_atoms().begin(
				Signature("_optimize", 1)); it != ctl.symbolic_atoms().end();
				it++) {
			optimize = it->symbol().arguments()[0].to_string();
		}
	}

	void print_preferences_tree() {
		this->_print_preferences_tree(this->optimize, "");
	}

	PreferenceNode& get_optimize() {
		return nodes[this->optimize];
	}

	PreferenceNode& get_node(std::string pref) {
		return nodes[pref];
	}

private:
	std::string optimize;
	std::unordered_map<std::string, PreferenceNode> nodes;
	void _print_preferences_tree(std::string pref, std::string level) {
		PreferenceNode node = nodes[pref];
		std::cout << level << "Preference " << node.name << " from type "
				<< node.type << "\n";
		if (node.theory_dependencies.size() > 0) {
			std::cout << level << " theories needed: ";
			for (std::string theory : node.theory_dependencies) {
				std::cout << theory << " ";
			}
			std::cout << "\n";
		}
		if (node.preference_dependencies.size() > 0) {
			std::cout << level << " depends on following preferences: \n";
			for (std::string subpref : node.preference_dependencies) {
				_print_preferences_tree(subpref, level + "  ");
			}
		}
	}
};

#endif /* LIBPREF_H_ */
