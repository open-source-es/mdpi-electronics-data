/*
	{{{ MIT License

	Copyright 2020 Kai Neubauer <kai.neubauer at uni-rostock.de> and Philipp Wanko <wanko at cs.uni-potsdam.de>

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
	associated documentation files (the "Software"), to deal in the Software without restriction, 
	including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
	and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
	subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial 
	portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
	LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
	IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE

	}}}
*/
#ifndef LIBTHEORY_H_
#define LIBTHEORY_H_

#include <clingo.hh>
#include <iostream>
#include <string>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <limits>
#include <chrono>
#include <iomanip>
#include <stdlib.h>
#include <memory>

using namespace Clingo;

/*
 * Abstract theory handling
 */

class Theory: public Propagator {
public:
	virtual void print_assignment(int thread) const =0;
	virtual void print_assignment_by_model(int model) const =0;
	virtual void save_model(int thread, int model)=0;
	virtual void print_stats(int thread)=0;
	virtual void print_stats()=0;
	virtual void set_config(char *argv[], int argc, Control &ctl)=0;

	virtual bool calc_approx(PropagateControl &ctl) = 0;
	virtual bool calc_exact(PropagateControl &ctl) = 0;
	virtual void remove_exact(Clingo::id_t thread) = 0;
	virtual void remove_approx(Clingo::id_t thread) = 0;

	virtual bool is_empty(Clingo::id_t thread) = 0;
};

class TheoryCreator {
public:
	virtual std::shared_ptr<Theory> create()=0;
};

class TheoryFactory {
public:
	static void register_theory(std::string theory,
			std::shared_ptr<TheoryCreator> theory_creator);
	static std::shared_ptr<Theory> get_theory(std::string theory);

private:
	static std::unordered_map<std::string, std::shared_ptr<TheoryCreator>>& getCreators() {
		static std::unordered_map<std::string, std::shared_ptr<TheoryCreator>> creators;
		return (creators);
	}

	static std::unordered_map<std::string, std::shared_ptr<Theory>>& getTheories() {
		static std::unordered_map<std::string, std::shared_ptr<Theory>> theories;
		return (theories);
	}

};

#endif /* LIBTHEORY_H_ */
