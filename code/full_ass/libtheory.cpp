/*
	{{{ MIT License

	Copyright 2020 Kai Neubauer <kai.neubauer at uni-rostock.de> and Philipp Wanko <wanko at cs.uni-potsdam.de>

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
	associated documentation files (the "Software"), to deal in the Software without restriction, 
	including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
	and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
	subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial 
	portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
	LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
	IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE

	}}}
*/
#include <clingo.hh>
#include <iostream>
#include <string>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <limits>
#include <chrono>
#include <iomanip>
#include <stdlib.h>
#include "libtheory.h"


//Declaration of available Theories
//std::unordered_map<std::string,std::shared_ptr<TheoryCreator>> TheoryFactory::creators;
//std::unordered_map<std::string,std::shared_ptr<Theory>> TheoryFactory::theories;

void TheoryFactory::register_theory(std::string theory, std::shared_ptr<TheoryCreator> theory_creator){
    std::unordered_map<std::string,std::shared_ptr<TheoryCreator>>::const_iterator got = TheoryFactory::getCreators().find(theory);
    if ( got == TheoryFactory::getCreators().end() ){
        TheoryFactory::getCreators()[theory] = theory_creator;
    }
    else{
        std::cout << "Warning: theory " << theory << " is already registered\n";
    }
}

std::shared_ptr<Theory> TheoryFactory::get_theory(std::string theory){
    std::unordered_map<std::string,std::shared_ptr<TheoryCreator>>::const_iterator got1 = TheoryFactory::getCreators().find(theory);
    if ( got1 == TheoryFactory::getCreators().end() ){
        return nullptr;
    }
    std::unordered_map<std::string,std::shared_ptr<Theory>>::const_iterator got = TheoryFactory::getTheories().find(theory);
    if ( got == TheoryFactory::getTheories().end() ){
        TheoryFactory::getTheories()[theory] = TheoryFactory::getCreators()[theory]->create();
    }
    return TheoryFactory::getTheories()[theory];
}


