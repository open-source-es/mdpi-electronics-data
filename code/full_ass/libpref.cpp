/*
	{{{ MIT License

	Copyright 2020 Kai Neubauer <kai.neubauer at uni-rostock.de> and Philipp Wanko <wanko at cs.uni-potsdam.de>

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
	associated documentation files (the "Software"), to deal in the Software without restriction, 
	including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
	and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
	subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial 
	portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
	LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
	IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE

	}}}
*/
#include <clingo.hh>
#include <iostream>
#include <string>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <limits>
#include <chrono>
#include <iomanip>
#include <stdlib.h>
#include "libtheory.h"
#include "libpref.h"
#include <memory>

//Declaration of available Preferences
//std::unordered_map<std::string,std::shared_ptr<PreferenceCreator>> PreferenceFactory::creators;
//std::map<std::pair<std::string,std::string>,std::shared_ptr<Preference>> PreferenceFactory::preferences;

void PreferenceFactory::register_preference(std::string type, std::shared_ptr<PreferenceCreator> preference_creator){
    std::unordered_map<std::string,std::shared_ptr<PreferenceCreator>>::const_iterator got = PreferenceFactory::getCreators().find(type);
    if ( got == PreferenceFactory::getCreators().end() ){
        PreferenceFactory::getCreators()[type] = preference_creator;
    }
    else{
        std::cout << "Warning: preference " << type << " is already registered\n";
    }
}

std::shared_ptr<Preference> PreferenceFactory::get_preference(std::string type,std::string name){
    std::unordered_map<std::string,std::shared_ptr<PreferenceCreator>>::const_iterator got1 = PreferenceFactory::getCreators().find(type);
    if ( got1 == PreferenceFactory::getCreators().end() ){
        return nullptr;
    }
    std::pair<std::string,std::string> key = std::make_pair(type,name);
    std::map<std::pair<std::string,std::string>,std::shared_ptr<Preference>>::const_iterator got2 = PreferenceFactory::getPreferences().find(key);
    if ( got2 == PreferenceFactory::getPreferences().end() ){
        PreferenceFactory::getPreferences()[key] = PreferenceFactory::getCreators()[key.first]->create(key.second);
    }
    return PreferenceFactory::getPreferences()[key];
}
