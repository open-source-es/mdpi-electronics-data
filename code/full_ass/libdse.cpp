/*
	{{{ MIT License

	Copyright 2020 Kai Neubauer <kai.neubauer at uni-rostock.de> and Philipp Wanko <wanko at cs.uni-potsdam.de>

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
	associated documentation files (the "Software"), to deal in the Software without restriction, 
	including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
	and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
	subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial 
	portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
	LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
	IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE

	}}}
*/
#include <utility>

#include "libdse.h"
#include "json.h"
#include <sys/mman.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <stdexcept>
#include <mutex>

void Latency::init(PropagateInit &init) {
	this->stats_.resize(init.number_of_threads());
	Timer t{stats_[0].init};

	//for each thread, there is a shm file with the name of the thread id
	this->handles.resize(init.number_of_threads());
	this->memory.resize(init.number_of_threads());
	for(int i = 0; i < init.number_of_threads(); i++){
		std::string str_tid = std::to_string(i);
		handles[i] = shm_open(str_tid.c_str(), O_CREAT | O_RDWR, 0777);
		//10485760 byte = 10 Mebibyte
		if(ftruncate(handles[i], 10485760))
			throw std::logic_error("Init failed. Couldn't create shared memory files!"); //every thread has 10 MB

		memory[i] = reinterpret_cast<char*>(mmap(nullptr, 10485760, PROT_READ | PROT_WRITE, MAP_SHARED, handles[i], 0));
	}
//	handle = shm_open("/shm", O_CREAT | O_RDWR, 0777);

	struct value {
		value(std::string atom, int task, int duration, int binding) :
				atom(std::move(atom)), task(task), duration(duration), binding(binding) {
		}

		std::string atom;
		int task;
		int duration;
		int binding;
	};

	enum class Port {
		HOME = 0,
		LEFT = 1,
		RIGHT = 2,
		TOP = 3,
		BOTTOM = 4,
		ILLEGAL = -1
	};

	struct routingvalue {
		routingvalue(std::string atom, int comm, int srcPID, int dstPID, bool begin, bool end, int port) :
				atom(std::move(atom)), comm(comm), srcPID(srcPID), dstPID(dstPID), port(port), begin(begin), end(end) {}

		std::string atom;
		int comm, srcPID, dstPID;
		int port;
		bool begin, end;
	};

	width_ = 0;
	height_ = 0;

	std::vector<value> values;
	std::vector<routingvalue> routingvalues;
	std::unordered_map<std::string, literal_t> symbol_table;
	// std::cout << "TOP:" << int(Port::TOP) <<"BOTTOM:" << int(Port::BOTTOM) <<"Right:" << int(Port::RIGHT) <<"LEFT:" << int(Port::LEFT) <<"HOME:" << int(Port::HOME) << std::endl;
	for (SymbolicAtomIterator it = init.symbolic_atoms().begin(); it != init.symbolic_atoms().end(); it++) {
		Symbol symbol = it->symbol();
//		std::cout << symbol << std::endl;
		if (std::strcmp(symbol.name(), "processor") == 0) {
			int pID = symbol.arguments()[0].number();
			int x = symbol.arguments()[1].number();
			int y = symbol.arguments()[2].number();
			processors[pID] = {x, y, symbol.to_string(), {}};
			width_ = std::max(width_, x);
			height_ = std::max(height_, y);
		}
		if (std::strcmp(symbol.name(), "comm") == 0) {
			int commID = idl_->get_id(symbol.to_string());
			int x = idl_->get_id(symbol.arguments()[0].to_string());
			int y = idl_->get_id(symbol.arguments()[1].to_string());
			comms[commID] = {x, y, symbol.to_string()};
		}
		if (std::strcmp(symbol.name(), "_preference") == 0 && symbol.arguments().size() == 5
			&& symbol.arguments()[0].to_string() == Preference::get_name() && std::strcmp(symbol.arguments()[3].name(), "name") != 0
			&& std::strcmp(symbol.arguments()[3].name(), "depends") != 0) {
			std::string atom = "_holds(" + symbol.arguments()[3].arguments()[0].to_string() + ",0)";
			int task = idl_->get_id(symbol.arguments()[4].arguments()[1].to_string());
			int pID = symbol.arguments()[1].arguments()[2].arguments()[3].arguments()[0].number();
			taskIdToName_[task] = symbol.arguments()[4].arguments()[1].to_string();
			tasks_.insert(task);
			int weight;
			if (symbol.arguments()[4].type() == SymbolType::Number) {
				weight = symbol.arguments()[4].number();
			} else {
				weight = symbol.arguments()[4].arguments()[0].number();
			}
			values.emplace_back(atom, task, weight, pID);

			// std::cout << "DSE Adding Task: " << symbol.arguments()[4].arguments()[1].to_string() << " with weight " << weight << std::endl;
		} else if (std::strcmp(symbol.name(), "_preference") == 0 && symbol.arguments().size() == 5
				   && symbol.arguments()[0].to_string() == "p2" && std::strcmp(symbol.arguments()[3].name(), "name") != 0
				   && symbol.arguments()[1].arguments()[1].number() == 3) {

			std::string atom = "_holds(" + symbol.arguments()[3].arguments()[0].to_string() + ",0)";
			auto name = symbol.arguments()[4].arguments()[1].arguments()[0].to_string();
			int comm = idl_->get_id(name);
//			std::cout << name << " : " << std::get<2>(comms[comm]) << std::endl;
			taskIdToName_[comm] = symbol.arguments()[4].arguments()[1].to_string();
			int src = symbol.arguments()[1].arguments()[2].arguments()[1].arguments()[0].number();
			int dst = symbol.arguments()[1].arguments()[2].arguments()[2].arguments()[0].number();
			bool begin = false, end = false;
			if (std::strcmp(symbol.arguments()[1].arguments()[2].arguments()[1].name(), "processor") == 0) {
				begin = true;
			} else if (std::strcmp(symbol.arguments()[1].arguments()[2].arguments()[2].name(), "processor") == 0) {
				end = true;
			}
			Port port;
			if (src == dst) {
				port = Port::HOME;
			} else {
				auto[srcX, srcY, dummy1, dummy3] = processors[src];
				auto[dstX, dstY, dummy2, dummy4] = processors[dst];
				if (srcX < dstX) {
					port = Port::RIGHT;
				} else if (srcX > dstX) {
					port = Port::LEFT;
				} else if (srcY < dstY) {
					port = Port::BOTTOM;
				} else if (srcY > dstY) {
					port = Port::TOP;
				}
			}
			routingvalues.emplace_back(atom, comm, src, dst, begin, end, int(port));
			// std::cout << "Port: " << int(port) << std::endl;
		} else {
			symbol_table[symbol.to_string()] = init.solver_literal(it->literal());
		}
	}

	for (auto &value : values) {
		if (symbol_table.find(value.atom) == symbol_table.end()) {
			std::cout << "Warning: atom " << value.atom << " does not appear in symbol table\n";
		} else {
//				init.add_watch( symbol_table[value.atom] );
			lit_to_val_[symbol_table[value.atom]].emplace_back(value.task, value.duration, value.binding);
		}
	}

	for (auto &value : routingvalues) {
		if (symbol_table.find(value.atom) == symbol_table.end()) {
			std::cout << "Warning: atom " << value.atom << " does not appear in symbol table\n";
		} else {
//				init.add_watch( symbol_table[value.atom] );
			lit_to_routeval_[value.comm].emplace_back(symbol_table[value.atom], value.srcPID, value.dstPID, value.begin, value.end,
													  int(value.port));
		}
	}

//	for (auto lit : idl_->get_watches()) {
//			init.add_watch( lit );
//	}

	this->initialize_states(init);
}

std::mutex var;

bool Latency::calc_exact(PropagateControl &ctl) {
	__FNC__;
	// auto t1  = std::chrono::steady_clock::now();
	json::Object jsonObject;
	json::Object tasksObject;
	json::Object messagesObject;
	json::Object processorsObject;
	auto &state = states_[ctl.thread_id()];

	auto level = ctl.assignment().decision_level();

	auto before = state.get_latency().val;
	if (!idl_->calc_exact(ctl)) {
		return false;
	}
	state.exact = true;
	ensure_decision_level(state, level + 1);

	//Create Source Route
	std::map<int, std::shared_ptr<std::vector<int>>> routes;
	for (const auto &comm : lit_to_routeval_) {
		auto hops = std::make_unique<std::deque<std::tuple<int, int, int>>>();
		auto msg = comms[comm.first];
		auto src_task = taskIdToName_[std::get<0>(msg)];
		auto dst_task = taskIdToName_[std::get<1>(msg)];

		auto src_processor = state.get_duration(std::get<0>(msg)).binding;
		auto dst_processor = state.get_duration(std::get<1>(msg)).binding;

		if (src_processor != dst_processor) {
			for (const auto &hop : comm.second) {
				if (ctl.assignment().is_true(std::get<0>(hop))) {
					auto[dummy, src, dst, begin, end, port] = hop;
					// std::cout << src << " " << dst << " "  << begin << " "  << end << " "  << port << " "  << std::endl;
					if (std::get<3>(hop) == true)
						hops->emplace_front(std::get<1>(hop), std::get<2>(hop), std::get<5>(hop));
					else if (std::get<4>(hop) == true)
						continue;
					else
						hops->emplace_back(std::get<1>(hop), std::get<2>(hop), std::get<5>(hop));
				}
			}
			//Sort the route.
			unsigned int pos = 0;
			auto cur = (*hops)[pos++];
			do {
				int next = std::get<1>(cur);
				for (auto it = hops->begin() + pos; it != hops->end(); it++) {
					if (std::get<0>(*it) == next) {
						std::iter_swap(hops->begin() + pos, it);
						cur = (*hops)[pos++];
						break;
					}
				}

			} while (pos < hops->size());

			//Delete first home port hop
			hops->pop_front();

			//Save the route
			json::Array route;
			for (auto elem : *hops) {
				route.push_back(std::get<2>(elem));
			}

			//Add the final hop to the home port
			route.push_back(0);

			//Write the message Object
			messagesObject[std::get<2>(msg)] = json::Array{src_task, dst_task, route};
		} else {
			messagesObject[std::get<2>(msg)] = json::Array{src_task, dst_task, json::Array()};
		}
	}

	std::unordered_map<int, std::vector<int>> bindings;
	for(const auto &processor : processors){
		bindings[processor.first] = {};
	}
	//Get the task bindings and durations
	for (int changed_task : tasks_) {
		int task_end = idl_->get_value(ctl.thread_id(), changed_task) + state.get_duration(changed_task).val;

		tasksObject[taskIdToName_[changed_task]] = json::Array{state.get_duration(changed_task).val};
		auto &binding = bindings[state.get_duration(changed_task).binding];
		binding.push_back(changed_task);
	}
	//Process and sort the binding for the json processor object
	for(auto &processor : processors){
		auto x = std::get<0>(processor.second);
		auto y = std::get<1>(processor.second);
		auto &name = std::get<2>(processor.second);
		auto &binding = bindings[processor.first];

		std::sort(binding.begin(), binding.end(), [this, &ctl](int &a, int &b){
			auto timeA = idl_->get_value( ctl.thread_id(), a );
			auto timeB = idl_->get_value( ctl.thread_id(), b );
			return timeA < timeB;
		});
		json::Array tasks;
		for(const auto &taskID : binding){
			tasks.push_back(taskIdToName_[taskID]);
		}

		processorsObject[name] = json::Array{x, y, tasks};
	}

#ifdef printAcc
	auto after = state.get_latency().val;
	std::cout << "Lantency: Exact done. " << after << std::endl;
	auto accuracy = (double)before / (double)after;
	std::cout << "Accuracy: " << accuracy << std::endl;
#endif

	//Put together the json object
	jsonObject["Tasks"] = tasksObject;
	jsonObject["Messages"] = messagesObject;
	jsonObject["Processors"] = processorsObject;
	jsonObject["Height"] = height_;
	jsonObject["Width"] = width_;
	auto jsonString = json::Serialize(jsonObject);

	//Put the string into shared memory

	memcpy(memory[ctl.thread_id()], jsonString.c_str(), jsonString.size());

	std::string str_size = std::to_string(jsonString.size());
	std::string str_tid = std::to_string(ctl.thread_id());


//		std::cout << ctl.thread_id() << " <-> " << str_size << std::endl;

	pid_t pid = fork();
//	std::cout << jsonString << std::endl;
	if (pid == 0){
		char *argv[]={const_cast<char *>("RosNoc"), const_cast<char *>(str_tid.c_str()), const_cast<char *>(str_size.c_str()), nullptr};
		execv("./rosnoc/RosNoc", argv);
		exit(127);
	}else{
		int status;
		waitpid(pid, &status, 0);
		if (WIFSIGNALED(status)){
			throw std::logic_error("NoC failed. " + std::to_string(ctl.thread_id()));
		}
//		std::cout << "child returned with status " << status << std::endl;
	}
//	std::string command = "./rosnoc/RosNocTest " + std::to_string(ctl.thread_id()) + " " +  std::to_string(jsonString.size());
//	auto ret = std::system(command.c_str());



	int after;
	memcpy(&after, memory[ctl.thread_id()], sizeof(after));

	state.get_latency().set_val( after );
	state.get_latency().set_task( -1 );
	if(after < before)
		throw std::logic_error("After is smaller than before."  + std::to_string(ctl.thread_id()) + " " + std::to_string(after) + " " + std::to_string(after - before));
//	std::cout << before << " " << after << std::endl;
	double accuracy = (double) before / (double) after;
//	munmap(memory, jsonString.size());
	std::lock_guard<std::mutex> lock(var);
	n++;
	auto delta = accuracy - mean_accuracy;
	mean_accuracy += delta / n;
	auto delta2 = accuracy - mean_accuracy;
	M2 = delta * delta2;
	// auto t2 = std::chrono::steady_clock::now();
	// std::cout << "Latency time:" << (t2-t1).count() << std::endl;
	return true;
}

void ParetoFront::unlink_shm(unsigned int thread) {
	std::string str_tid = std::to_string(thread);
	shm_unlink(str_tid.c_str());
}