/*
	{{{ MIT License

	Copyright 2020 Kai Neubauer <kai.neubauer at uni-rostock.de> and Philipp Wanko <wanko at cs.uni-potsdam.de>

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
	associated documentation files (the "Software"), to deal in the Software without restriction, 
	including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
	and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
	subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial 
	portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
	LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
	IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE

	}}}
*/
#include <clingo.hh>
#include <iostream>
#include <string>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <limits>
#include <chrono>
#include <iomanip>
#include <stdlib.h>
#include "libutil.h"
#include "libtheory.h"
#include <memory>
#include <csignal>
#include <mutex>
#include <shared_mutex>
#include <thread>
#include "libpref.h"
#include "libdse.h"
//#include <sys/mman.h>

//#define CROSSCHECK
#define CHECKSOLUTION

using namespace Clingo;


// Mutex for multi-threading
std::shared_timed_mutex write;
int approxhelped = 0;
int total =0 ;
/*
 * Type definitions for the controller
 */

// Preference handling
class PreferencePropagator : public Propagator {
public:
    PreferencePropagator(){
        update_ = true;
        enumerate_ = false;
    }
    PreferencePropagator(std::shared_ptr<Preference> pref){
        this->set_lead(pref);
    }

    void save_model(unsigned int thread, unsigned int model){
        std::lock_guard<std::shared_timed_mutex> lock(write);
        lead->save_model(thread,model);
//        lead->remove_exact(thread);
//        lead->remove_approx(thread);
    }
    void print_quality(unsigned int model){
        lead->print_quality(model);
    }
    void print_stats(){
        lead->print_stats();
    }
    void set_lead(std::shared_ptr<Preference> pref){
        lead = pref;
    }
    void compare_to(int model){
        std::lock_guard<std::shared_timed_mutex> lock(write);
        compare_ = model;
    }
    void set_update(bool update){
        this->update_ = update;
    }

    std::vector<int> current_best(){
        std::shared_lock<std::shared_timed_mutex> lock(write);
        return lead->current_best();
    }

    void optimum_found(){
        std::lock_guard<std::shared_timed_mutex> lock(write);
        lead->optimum_found();
    }

    void enumerate(){
        enumerate_ = true;
    }

    void set_approx(bool approx){
        approx_ = approx;
    }

    void set_wait(unsigned int ms){
        wait_for_ = std::chrono::microseconds(ms);
        wait_ = true;
    }

private:
    std::shared_ptr<Preference> lead;
    int compare_;
    bool update_;
    bool enumerate_;
    bool approx_ = false;
    bool wait_ = false;
    std::chrono::microseconds wait_for_ = std::chrono::microseconds(0);
    void add_conflict_clause(PropagateControl &ctl, std::vector<literal_t>& reason){
        std::vector<literal_t> clause;
		clause.reserve(reason.size());
		for (auto lit : reason) {
            clause.emplace_back(-lit);
        }
        ClauseType type;
        if (enumerate_){
            type = ClauseType::Volatile;
        }
        else {
            type = ClauseType::Learnt;
        }
        if (!ctl.add_clause(clause,type) || !ctl.propagate()) {
            return;
        }
        //throw std::logic_error("Must not happen");
        std::cout << "must not happen Thread " << ctl.thread_id() << std::endl;
        assert(false && "must not happen");

    }

    // initialization

    void init(PropagateInit &init) override {
        if (update_){
            lead->init(init);
        }
    }

    // propagation

    void propagate(PropagateControl &ctl, LiteralSpan changes) override {
        std::shared_lock<std::shared_timed_mutex> lock(write);
        lead->propagate(ctl, changes);
        if (lead->worse(ctl.thread_id(),compare_)){
            std::vector<literal_t> reason = lead->reason(ctl);
            add_conflict_clause(ctl,reason);
        }
    }

    // undo

    void undo(PropagateControl const &ctl, LiteralSpan changes) override {
        lead->undo(ctl,changes);
    }
    void check(PropagateControl &ctl) override {
        //TODO: is this mutex necessary?
    	std::shared_lock<std::shared_timed_mutex> lock(write);
//		std::lock_guard<std::shared_timed_mutex> lock(write);
        lead->remove_exact(ctl.thread_id());
        lead->remove_approx(ctl.thread_id());
		total++;
        if(!lead->calc_approx(ctl)){
            lead->remove_approx(ctl.thread_id());
            approxhelped++;
            return;
        }else if (approx_ && !lead->better(ctl.thread_id(), compare_)){ //compare is not used in "Pareto Front"
            std::vector<literal_t> reason = lead->reason(ctl);
            add_conflict_clause(ctl,reason);
            approxhelped++;
            return;
        }
        //Approx is not dominated!
        if(wait_){
            std::this_thread::sleep_for(wait_for_);
        }
        if(!lead->calc_exact(ctl)){
            return;
        } else if(!lead->better(ctl.thread_id(), compare_)){
            std::vector<literal_t> reason = lead->reason(ctl);
            add_conflict_clause(ctl,reason);
            return;
        }
        //Exact is not dominated!
    }
};


// Statistics
struct Stats {
    Duration time_total = Duration{0};
    Duration time_init = Duration{0};
    int64_t conflicts{0};
    int64_t choices{0};
    int64_t restarts{0};
};

/*
 * Initialization
 */

Stats stats;
Timer* t;

// Theories that are currently used
std::vector<std::string> theories;
// Recorded models
std::vector<std::vector<Symbol>> models;
// Preference propagator
PreferencePropagator pref_prop;
bool verbose;
bool approx;
unsigned int wait = 0;

// Define control object
Control ctl;

/*
 * Controller functions
 */

// Registers a theory
void register_theory(int argc, char *argv[], Control &ctl, std::string theory){
    std::vector<std::string>::iterator it;

    it = find (theories.begin(), theories.end(), theory);
    if (it == theories.end()){
        std::shared_ptr<Theory> theory_ptr = TheoryFactory::get_theory(theory);
        if (theory_ptr == nullptr){
        	std::cout << "Warning: Theory " << theory << " not found.\n";
        }
        theory_ptr->set_config(argv,argc,ctl);
        ctl.register_propagator(*theory_ptr);
        theories.emplace_back(theory);
    }
}

// Print statistics
void print_stats(){
	pref_prop.print_stats();
	for (auto &theory : theories){
		TheoryFactory::get_theory(theory)->print_stats();
	}
}

// Print currently best known solutions and statistics in case of interrupt
void print_solutions_interrupted(int signum){
    int n = 0;

    ctl.interrupt();

    std::cout << "INTERRUPTED! Printing currently best solutions." << std::endl;

    for (int i : pref_prop.current_best()){
        n++;
        std::cout << "Answer "<< n << "\n";
        std::cout << models[i] << "\n";
        for (auto &theory : theories){
            TheoryFactory::get_theory(theory)->print_assignment_by_model(i);
        }
        pref_prop.print_quality(i);
    }
	std::cout << "Total models evaluated    : " << total << std::endl;
    std::cout << "Filtered by Approximation : " << approxhelped << std::endl;
	std::cout << "Filtered by Exact Calc.   : " << total - approxhelped << std::endl;
	std::cout << "Ratio                     : " << (double)approxhelped / (double)total << std::endl;

    delete t;
    std::cout << "total time: " << stats.time_total.count() << "s\n";

    if (verbose){
        print_stats();
    }
    auto solvers = ctl.statistics()["solving"]["solvers"];
    stats.choices = solvers["choices"];
    stats.conflicts = solvers["conflicts"];
    stats.restarts = solvers["restarts"];

    std::cout << "conflicts: " << stats.conflicts << "\n";
    std::cout << "choices  : " << stats.choices << "\n";
    std::cout << "restarts : " << stats.restarts << "\n";

    exit(signum);
}

// Assemble lead preference object
std::shared_ptr<Preference> instantiate_pref(int argc, char *argv[], Control &ctl, PreferenceNode &node, PreferenceGraph &graph){
    std::shared_ptr<Preference> pref = PreferenceFactory::get_preference(node.type,node.name);
    if (pref == nullptr){
    	std::cout << "ERROR: Preference " << node.type << " not found.\n";
    	exit(-1);
    }
    pref->set_config(argv,argc,ctl);
    for (auto& theory : node.theory_dependencies){
        register_theory(argc,argv,ctl,theory);
    }
    for (auto& subpref : node.preference_dependencies){
        PreferenceNode& subnode = graph.get_node(subpref);
        pref->add_subpref(instantiate_pref(argc, argv, ctl, subnode, graph));
    }
    return pref;
}


// Miscellaneous
template<typename Out>
void split(const std::string &s, char delim, Out result) {
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}


/*
 * MAIN
 */

int main(int argc, char *argv[]) {
    std::signal(SIGINT, print_solutions_interrupted);
	std::signal(SIGTERM, print_solutions_interrupted);
//	std::cout << "Test34" << std::endl;
    verbose = false;
    for (auto arg = argv + 1; arg != argv + argc; ++arg) {
        if (std::strcmp(*arg, "-v") == 0) {
            verbose = true;
        }
        else if (std::strcmp(*arg, "-a") == 0) {
            pref_prop.set_approx(true);
        }
        else if (std::string(*arg).find("-w=") == 0) {
            int time = std::stoi(split(std::string(*arg), '=')[1]);
            pref_prop.set_wait(time);
        }
        std::cout << arg << std::endl;
    }

    t = new Timer(stats.time_total);
    auto argb = argv + 1, arge = argb;
    for (; *argb; ++argb, ++arge) {
        if (std::strcmp(*argb, "--") == 0) {
            ++argb;
            break;
        }
    }

    int max_models = 1;
    for (auto arg = argv + 1; arg != arge; ++arg) {
        if (std::string(*arg).find("-n=") == 0) {
            max_models = std::stoi(split(std::string(*arg), '=')[1]);
        }
    }

    ctl = Control{{argb, numeric_cast<size_t>(argv + argc - argb)}};
    for (auto arg = argv + 1; arg != arge; ++arg) {
        if (std::string(*arg).find("-") != 0) {
            ctl.load(*arg);
        }
    }


    for (auto &def_theory : get_tuple("theories", ctl, {})){
    	register_theory(argc,argv,ctl,def_theory);
    }


    ctl.ground({{"base", {}}});
    ctl.ground({{"specification", {}}});
    PreferenceGraph prefs = PreferenceGraph(ctl);
    prefs.print_preferences_tree();
    std::shared_ptr<Preference> lead = instantiate_pref(argc, argv, ctl, prefs.get_optimize(), prefs);
    pref_prop.set_lead(lead);
    ctl.register_propagator(pref_prop);
    bool sat;
    unsigned int i = 0;
    int opt_models = 0;
    bool unsat = false;
    if (max_models != 1){
        pref_prop.enumerate();
    }
    while(true){
        sat = false;
        std::cout << "Solving...\n";
        auto tmp = ctl.solve();
        for (auto &m : tmp) {
            sat = true;
            pref_prop.save_model(m.thread_id(),i);
            for (auto &theory : theories){
                TheoryFactory::get_theory(theory)->save_model(m.thread_id(),i);
            }
            models.emplace_back(m.symbols(ShowType::Shown));
            i++;
            break;
        }
        if (!sat && i == 0) {
            std::cout << "UNSATISFIABLE\n";
            break;
        }
        else if (!sat){
            if (unsat){
                break;
            }
            unsat = true;
            std::cout << "OPTIMUM FOUND\n";
            opt_models++;
            if (opt_models >= max_models && max_models != 0){
                break;
            }
            pref_prop.optimum_found();
        }
        else {
            {
                std::lock_guard<std::shared_timed_mutex> lock(write);
                std::cout << "ANSWER FOUND " << i <<"\n";
                unsat = false;
                std::cout << models[i - 1] << "\n";
                for ( auto& theory : theories ) {
                    TheoryFactory::get_theory(theory)->print_assignment_by_model(i - 1);
                }
                pref_prop.print_quality(i-1);
                std::cout << "Total models evaluated    : " << total << std::endl;
                std::cout << "Filtered by Approximation : " << approxhelped << std::endl;
                std::cout << "Filtered by Exact Calc.   : " << total - approxhelped << std::endl;
                std::cout << "Ratio                     : " << (double)approxhelped / (double)total << std::endl;
            }
            pref_prop.compare_to(i-1);
            if (i == 1){
            	pref_prop.set_update(false);
            }
        }
    }
    if (i>0){
		int n = 0;
		for (int m : pref_prop.current_best()){
			n++;
			std::cout << "Answer "<< n << "\n";
			std::cout << models[m] << "\n";
			for (auto &theory : theories){
				TheoryFactory::get_theory(theory)->print_assignment_by_model(m);
			}
			pref_prop.print_quality(m);
		}
    }

    auto solvers = ctl.statistics()["solving"]["solvers"];
    stats.choices = solvers["choices"];
    stats.conflicts = solvers["conflicts"];
    stats.restarts = solvers["restarts"];

    delete t;
    std::cout << "total: " << stats.time_total.count() << "s\n";
    if (verbose){
    	print_stats();
    }

    std::cout << "conflicts: " << stats.conflicts << "\n";
    std::cout << "choices  : " << stats.choices << "\n";
    std::cout << "restarts : " << stats.restarts << "\n";
}
