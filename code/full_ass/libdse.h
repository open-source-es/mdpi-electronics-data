/*
	{{{ MIT License

	Copyright 2020 Kai Neubauer <kai.neubauer at uni-rostock.de> and Philipp Wanko <wanko at cs.uni-potsdam.de>

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
	associated documentation files (the "Software"), to deal in the Software without restriction, 
	including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
	and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
	subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial 
	portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
	LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
	IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE

	}}}
*/
#ifndef LIDSE_H_
#define LIDSE_H_
#ifdef _MSC_VER
#define __PRETTY_FUNCTION__ ""
#endif
#ifdef FNC
#define __FNC__ std::cout << __PRETTY_FUNCTION__ << std::endl
#else
#define __FNC__
#endif
#include <clingo.hh>
#include <iostream>
#include <string>
#include <tuple>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <deque>
#include <limits>
#include <chrono>
#include <iomanip>
#include <stdlib.h>
#include <memory>
#include <unordered_set>
#include "libpref.h"
#include "libdl.h"


using namespace Clingo;

struct PreferenceStats {
	Duration save_model = Duration { 0 };
	Duration better = Duration { 0 };
	Duration worse = Duration { 0 };
	Duration reason = Duration { 0 };
	Duration propagate = Duration { 0 };
	Duration init = Duration { 0 };
	Duration undo = Duration { 0 };
	Duration exact = Duration { 0 };
	Duration approx = Duration { 0 };
};

/*
 *  Pareto front
 */

class ParetoFront : public Preference {
public:
	ParetoFront( std::string name ) :
			Preference( name ) {
		best_ = {};
		removal_candidates_ = {};
	}
	void print_quality(unsigned int model) override {
		std::cout << "Pareto " << Preference::get_name()<< ":\n";
		for (const auto &subpref : Preference::get_subprefs()) {
			subpref->print_quality(model);
		}
	}

	void print_stats(int thread) override {
		auto stat = stats_[thread];
		std::cout << "  paretoFront "<<Preference::get_name()<<"\n";
		std::cout << "  total[" << thread << "]: ";
		std::cout << (stat.undo + stat.propagate + stat.save_model+stat.better+stat.worse+stat.reason+stat.approx+stat.exact).count() << "s\n";
		std::cout << "    init       : " << stat.init.count() << "s\n";
		std::cout << "    propagate  : " << stat.propagate.count() << "s\n";
		std::cout << "    save model : " << stat.save_model.count() << "s\n";
		std::cout << "    better     : " << stat.better.count() << "s\n";
		std::cout << "    worse      : " << stat.worse.count() << "s\n";
		std::cout << "    reason     : " << stat.reason.count() << "s\n";
		std::cout << "    undo       : " << stat.undo.count() << "s\n";
		std::cout << "    exact      : " << stat.exact.count() << "s\n";
		std::cout << "    approx     : " << stat.approx.count() << "s\n";

	}

	void set_config(char *argv[], int argc, Control &ctl) override {
		(void)argv; (void)argc; (void)ctl;
	}

	void print_stats() override {
		for(unsigned int thread = 0; thread < stats_.size(); thread++) {
			print_stats(thread);

			//unload shared memory
			unlink_shm(thread);
		}
		for (const auto &pref : Preference::get_subprefs()) {
			pref->print_stats();
		}
	}

	void unlink_shm(unsigned int thread);

	void save_model(unsigned int thread, unsigned int model) override {
		Timer t {stats_[thread].save_model};
		for (const auto &subpref : Preference::get_subprefs()) {
			subpref->save_model(thread,model);
		}
		for (auto cand : removal_candidates_[thread]) {
			auto nend = std::remove(best_.begin(), best_.end(), cand);
			best_.erase(nend, best_.end());
		}
		best_.emplace_back(model);
	}

	int dominated(unsigned int thread, unsigned int model) {
		bool better = false;
		bool worse = false;
		for (const auto &subpref : Preference::get_subprefs()) {
			if (!better && subpref->better(thread,model)) {
				better = true;
			}
			if (!worse && subpref->worse(thread,model)) {
				worse = true;
			}
		}
		if (better && worse) {
			return 0;
		}
		else if (better) {
			return 1;
		}
		else {
			return -1;
		}
	}

	bool better(unsigned int thread, unsigned int model) override {
		(void) model;
		Timer t {stats_[thread].better};
		if (best_.empty()) {
			return true;
		}
		removal_candidates_[thread] = {};
		for (auto prev_model : best_) {
			int dom = dominated(thread, prev_model);
			if (dom < 0) {
				return false;
			}
			else if (dom == 1) {
				removal_candidates_[thread].emplace_back(prev_model);
			}
		}
		return true;
	}
	bool bettereq(unsigned int thread, unsigned int model) override {
		return !worse(thread,model);
	}
	bool worse(unsigned int thread, unsigned int model) override {
		(void) model;
		Timer t {stats_[thread].worse};
		if (best_.empty()) {
			return false;
		}
		for (auto prev_model : best_) {
			int dom = dominated(thread,prev_model);
			if (dom < 0) {
				return true;
			}
		}
		return false;
	}
	std::vector<literal_t> reason(PropagateControl &ctl) override {
		Timer t {stats_[ctl.thread_id()].reason};
		std::vector<int> res = {};
		std::vector<literal_t> reason;
		for (const auto &subpref : Preference::get_subprefs()) {
			reason = subpref->reason(ctl);
			for(auto lit : reason) {
				assert(ctl.assignment().is_true(lit));
			}
			res.insert(res.end(), reason.begin(), reason.end());
		}
		return res;
	}
	std::vector<int> current_best() override {
	    return best_;
	}

    bool is_empty(Clingo::id_t thread) override {
        for (const auto &subpref : Preference::get_subprefs()){
            if (!subpref->is_empty(thread)) {
                return false;
            }
        }
	    return true;
    }

    bool calc_approx(PropagateControl &ctl) override {
        __FNC__;
		Timer t{stats_[ctl.thread_id()].approx};
	    for (const auto &subpref : Preference::get_subprefs()) {
            if(!subpref->calc_approx(ctl)){
//                remove_approx(ctl.thread_id());
                return false;
            };
        }
        return true;
	}

	bool calc_exact(PropagateControl &ctl) override {
        __FNC__;
		Timer t{stats_[ctl.thread_id()].exact};
	    for (const auto &subpref : Preference::get_subprefs()) {
            if(!subpref->calc_exact(ctl)){
//                remove_exact(ctl.thread_id());
                return false;
            };
        }
        return true;
	}

	void remove_exact(Clingo::id_t thread) override {
        __FNC__;
	    for (const auto &subpref : Preference::get_subprefs()) {
            subpref->remove_exact(thread);
        }
	}

	void remove_approx(Clingo::id_t thread) override {
        __FNC__;
	    for (const auto &subpref : Preference::get_subprefs()) {
            subpref->remove_approx(thread);
        }
	}

private:
	// initialization

	void init(PropagateInit &init) override {
		stats_.resize(static_cast<unsigned long>(init.number_of_threads()));
		for (int thread=0; thread<init.number_of_threads(); thread++) {
			removal_candidates_[thread] = {};
		}
		Timer t {stats_[0].init};
		for (const auto &subpref : Preference::get_subprefs()) {
			subpref->init(init);
		}
	}

	// propagation

	void propagate(PropagateControl &ctl, LiteralSpan changes) override {
		Timer t {stats_[ctl.thread_id()].propagate};
		for (const auto &subpref : Preference::get_subprefs()) {
			subpref->propagate(ctl,changes);
		}
	}

	// undo

	void undo(PropagateControl const &ctl, LiteralSpan changes) override {
		Timer t {stats_[ctl.thread_id()].undo};
		for (const auto &subpref : Preference::get_subprefs()) {
			subpref->undo(ctl,changes);
		}
	}

	void check( PropagateControl &ctl ) override {
		for (const auto &subpref : Preference::get_subprefs()) {
			subpref->check(ctl);
		}
	}

	bool composite = true;
	std::vector<int> best_;
	std::unordered_map<int,std::vector<int>> removal_candidates_;
	std::vector<PreferenceStats> stats_;
};

class ParetoFrontCreator : public PreferenceCreator {
public:
	std::shared_ptr<Preference> create( std::string name ) override {
		return std::make_shared<ParetoFront>( ParetoFront( name ) );
	}
};

class ParetoFrontRegistration {
public:
	ParetoFrontRegistration() {
		std::shared_ptr<PreferenceCreator> c;
		c.reset( new ParetoFrontCreator() );
		PreferenceFactory::register_preference( "paretoFront", c );
	}
};

static ParetoFrontRegistration pareto_front;

/*
 *  Latency
 */

struct duration {
	duration( int decision_lvl, literal_t lit, int val, int binding ) :
			decision_lvl( decision_lvl ), lit( lit ), val( val ), binding (binding) {
	}
	int decision_lvl;
	literal_t lit;
	void set_lit( literal_t nlit ) {
		lit = nlit;
	}

	int val;
	void set_val( int nval ) {
		val = nval;
	}
	int binding; //Processor ID
	void set_bind( int nbind ) {
		binding = nbind;
	}
};

struct latency {
	latency( int decision_lvl, int task, int val ) :
			decision_lvl( decision_lvl ), task( task ), val( val ) {
	}
	int decision_lvl;
	int task;
	void set_task( int ntask ) {
		task = ntask;
	}
	int val;
	void set_val( int nval ) {
		val = nval;
	}
};

struct LatencyState {
	std::unordered_map<int, std::vector<duration>> duration_trail;
	std::vector<latency> latency_trail;
	latency &get_latency() {
		return latency_trail.back();
	}
	duration &get_duration( int task ) {
		return duration_trail[task].back();
	}
	bool exact = false;
	bool approximated = false;
};

class Latency : public Preference {
public:
	Latency( std::string name ) :
			Preference( name ) {
		std::shared_ptr<Theory> theory = TheoryFactory::get_theory( "idl" );
		idl_ = std::dynamic_pointer_cast<DifferenceLogicPropagator<int>>( theory );
		best_ = -1;
//        exact_ = false;
	}
	void print_quality( unsigned int model ) override {
		std::cout << "Latency " << Preference::get_name() << ": " << archive_[model] << "\n";
	}

	void print_stats( int thread ) override {
		auto stat = stats_[thread];
		std::cout << "  latency " << Preference::get_name() << "\n";
		std::cout << "  total[" << thread << "]: ";
		std::cout << ( stat.undo + stat.propagate + stat.save_model + stat.better + stat.worse + stat.reason ).count() << "s\n";
		std::cout << "    init       : " << stat.init.count() << "s\n";
		std::cout << "    propagate  : " << stat.propagate.count() << "s\n";
		std::cout << "    save model : " << stat.save_model.count() << "s\n";
		std::cout << "    better     : " << stat.better.count() << "s\n";
		std::cout << "    worse      : " << stat.worse.count() << "s\n";
		std::cout << "    reason     : " << stat.reason.count() << "s\n";
		std::cout << "    undo     : " << stat.undo.count() << "s\n";

	}

	void set_config( char *argv[], int argc, Control &ctl ) override {
		(void) ctl;
		bool reason_min = false;
		for ( auto arg = argv + 1; arg != argv + argc; ++arg ) {
			if ( std::strcmp( *arg, "-r" ) == 0 ) {
				reason_min = true;
			}
		}
		// configure if reason minimization is enabled
		this->reason_min = reason_min;
	}

	void print_stats() override {
		std::cout << "Accuracy                  : " << mean_accuracy << std::endl;
		std::cout << "Variance                  : " << M2/n << std::endl;
		for ( unsigned int thread = 0; thread < stats_.size(); thread++ ) {
			print_stats( thread );
		}
	}

	void save_model( unsigned int thread, unsigned int model ) override {
		Timer t { stats_[thread].save_model };
		auto &state = states_[thread];
		std::cout << "Save model: Thread" << thread << std::endl;
		archive_.resize( model + 1 );
		archive_[model] = state.get_latency().val;
		if ( best_ == -1 || state.get_latency().val < best_val_ ) {
			best_ = model;
			best_val_ = state.get_latency().val;
		}
	}

	bool better( unsigned int thread, unsigned int model ) override {
		Timer t { stats_[thread].better };
		if (archive_.empty()) {
			return true;
		}
		auto &state = states_[thread];
		return state.get_latency().val < archive_[model];
	}

	bool bettereq( unsigned int thread, unsigned int model ) override {
		if (archive_.empty()) {
			return true;
		}
		auto &state = states_[thread];
		return state.get_latency().val <= archive_[model];
	}

	bool worse( unsigned int thread, unsigned int model ) override {
		Timer t { stats_[thread].worse };
		if (archive_.empty()) {
			return false;
		}
		auto &state = states_[thread];
		return state.get_latency().val > archive_[model];
	}

	std::vector<literal_t> reason( PropagateControl &ctl ) override {
        __FNC__;
	    Timer t { stats_[ctl.thread_id()].reason };
		auto &state = states_[ctl.thread_id()];
		DifferenceLogicGraph<int>& dl_graph = idl_->get_graph( ctl.thread_id() );
		std::unordered_set<literal_t> res_set;
		if ( reason_min && ( state.get_latency().val != -1 ) ) {
			int max_task = state.get_latency().task;
			res_set.insert( state.get_duration( max_task ).lit );
			std::vector<int> edge_stack = dl_graph.get_node( max_task ).incoming;
			std::unordered_set<int> visited( edge_stack.begin(), edge_stack.end() );
			int cur_edge;
			while (!edge_stack.empty()) {
				cur_edge = edge_stack.back();
				Edge<int> edge = dl_graph.get_edge( cur_edge );
				edge_stack.pop_back();
				res_set.insert( edge.lit );
				for ( int in : dl_graph.get_node( edge.from ).incoming ) {
					std::unordered_set<int>::const_iterator got = visited.find( in );
					if ( got == visited.end() ) {
						edge_stack.emplace_back( in );
						visited.insert( in );
					}
				}
			}
		} else {
			for ( auto task : state.duration_trail ) {
				if ( task.second.back().lit != 0 ) {
					res_set.insert( task.second.back().lit );
					assert(ctl.assignment().is_true(task.second.back().lit));
				}
			}
			for ( auto &node : dl_graph.get_nodes() ) {
				for ( auto &edge : node.outgoing ) {
					res_set.insert( dl_graph.get_edge( edge ).lit );
					assert(ctl.assignment().is_true(dl_graph.get_edge( edge ).lit ));
				}
			}
		}

		std::vector<literal_t> res( res_set.begin(), res_set.end() );

		return res;
	}

    bool is_empty(Clingo::id_t thread) override {
        auto& state = states_[thread];
        if(!idl_->is_empty(thread)){
            return false;
        }
	    return state.latency_trail.empty();
    }

    bool calc_approx(PropagateControl &ctl) override{
        __FNC__;
	    auto& state = states_[ctl.thread_id()];
//        std::cout << "Latency: Check Approx " << ctl.thread_id() << std::endl;
        auto level = ctl.assignment().decision_level();
        ensure_decision_level(state, level);
        state.approximated = true;
        //First get the duration of the tasks according to the binding
        for (const auto &lit : lit_to_val_ ) {
            if(ctl.assignment().is_true(lit.first)){
                for ( auto &duration : lit.second ) {
                    int task = std::get<0>(duration);
                    int val = std::get<1>(duration);
                    int bind = std::get<2>(duration);
                    state.get_duration( task ).set_lit( lit.first );
                    state.get_duration( task ).set_val( val );
                    state.get_duration( task ).set_bind( bind );
                }
            }
        }

        //Shortest Path Difference Logic
        if(!idl_->calc_approx(ctl)){
            return false;
        }

        //Calculate Latency approximately
        for ( int changed_task : tasks_ ) {
            int task_end = idl_->get_value( ctl.thread_id(), changed_task ) + state.get_duration( changed_task ).val;
            if ( ( state.get_latency().val < 0 || task_end > state.get_latency().val ) && state.get_duration( changed_task ).val != -1 ) {
                state.get_latency().set_val( task_end );
                state.get_latency().set_task( changed_task );
            }
        }

        return true;
	}

	bool calc_exact(PropagateControl &ctl) override;

	void remove_exact(Clingo::id_t thread) override{
        __FNC__;
	    auto& state = states_[thread];
	    idl_->remove_exact(thread);
        if(state.exact){
            state.exact = false;
//			std::cout << "libdse undo; Removing Exact trace! Thread " << ctl.thread_id() << std::endl;
            for ( auto task : tasks_ ) {
                state.duration_trail[task].pop_back();
            }
            state.latency_trail.pop_back();
        }
	}

	void remove_approx(Clingo::id_t thread) override {
        __FNC__;
	    auto& state = states_[thread];
		idl_->remove_approx(thread);
		if ( state.approximated ) {
			state.approximated = false;
			for ( auto task : tasks_ ) {
				state.duration_trail[task].pop_back();
				assert(state.duration_trail[task].empty());
			}
			state.latency_trail.pop_back();
		}
        assert(state.latency_trail.empty());
	}

	std::vector<int> current_best() override {
		return {best_};
	}
	std::tuple<double,double> getAccuracy(){
		return std::make_tuple(mean_accuracy, M2/n);
	}
private:

	void initialize_states( PropagateInit &init ) {
		states_.resize( init.number_of_threads() );
		for ( int i = 0; i < init.number_of_threads(); ++i ) {
			states_.emplace_back();
		}
	}

	// initialization

	void init( PropagateInit &init ) override;


	void ensure_decision_level( LatencyState &state, int level ) {
		if (state.latency_trail.empty()) {
			for ( auto task : tasks_ ) {
				state.duration_trail[task].emplace_back( level, 0, -1, -1 );
			}
			state.latency_trail.emplace_back( level, -1, -1 );
		} else if ( state.get_latency().decision_lvl < level ) {
			for ( auto task : tasks_ ) {
				state.duration_trail[task].emplace_back( level, state.get_duration( task ).lit, state.get_duration( task ).val, state.get_duration( task ).binding );
			}
			state.latency_trail.emplace_back( level, state.get_latency().task, state.get_latency().val );
		}
	}

	// propagation

	void propagate( PropagateControl &ctl, LiteralSpan changes ) override {
		Timer t { stats_[ctl.thread_id()].propagate };
		return;
		auto &state = states_[ctl.thread_id()];
		auto level = ctl.assignment().decision_level();
		ensure_decision_level( state, level );

		// std::cout << "libdse propagate" << std::endl;

		for ( auto lit : changes ) {
			auto it = lit_to_val_.find( lit );
			if ( it != lit_to_val_.end() ) {
				for ( auto &duration : lit_to_val_[lit] ) {
					int task = std::get<0>(duration);
					int val = std::get<1>(duration);
					state.get_duration( task ).set_lit( lit );
					state.get_duration( task ).set_val( val );
				}
			}
		}

		for ( int changed_task : tasks_ ) {
			int task_end = idl_->get_value( ctl.thread_id(), changed_task ) + state.get_duration( changed_task ).val;
			if ( ( state.get_latency().val < 0 || task_end > state.get_latency().val ) && state.get_duration( changed_task ).val != -1 ) {
				state.get_latency().set_val( task_end );
				state.get_latency().set_task( changed_task );
			}
		}
		// if ( state.get_latency().val == -1 ) { return; }
		// else { std::cout << "Current Latency: " << state.get_latency().val << std::endl; }
	}

	// undo

	void undo( PropagateControl const &ctl, LiteralSpan changes ) override {
		(void) changes;
		std::cout << "Latency: Undo" << std::endl;
		Timer t { stats_[ctl.thread_id()].undo };
		auto &state = states_[ctl.thread_id()];
		for ( auto task : tasks_ ) {
			state.duration_trail[task].pop_back();
		}
        state.latency_trail.pop_back();
		if ( state.exact ) {
			state.exact = false;
//			std::cout << "libdse undo; Removing Exact trace! Thread " << ctl.thread_id() << std::endl;
            for ( auto task : tasks_ ) {
                state.duration_trail[task].pop_back();
            }
            state.latency_trail.pop_back();
			idl_->remove_exact( ctl.thread_id() );
		}
		if ( state.approximated ) {
            state.approximated = false;
            for ( auto task : tasks_ ) {
                state.duration_trail[task].pop_back();
            }
            state.latency_trail.pop_back();
            idl_->remove_approx(ctl.thread_id());
        }
	}

	// check

	void check( PropagateControl& ctl ) override {
        __FNC__;
	    auto& state = states_[ctl.thread_id()];
//        std::cout << "Latency: Check" << ctl.thread_id() << std::endl;
        auto level = ctl.assignment().decision_level();
        ensure_decision_level(state, level);
        //First get the duration of the tasks according to the binding
        for ( auto lit : lit_to_val_ ) {
            if(ctl.assignment().is_true(lit.first)){
                for ( auto &duration : lit.second ) {
                    int task = std::get<0>(duration);
                    int val = std::get<1>(duration);
                    state.get_duration( task ).set_lit( lit.first );
                    state.get_duration( task ).set_val( val );
                }
            }
        }

        //Shortest Path Difference Logic
		if(!idl_->calc_approx(ctl)){
			undo(ctl,{});
		    return;
		}
        state.approximated = true;
		//Calculate Latency approximately

		ensure_decision_level( state, level + 1 );
		for ( int changed_task : tasks_ ) {
			int task_end = idl_->get_value( ctl.thread_id(), changed_task ) + state.get_duration( changed_task ).val;
			std::cout << idl_->get_name(changed_task) << ":" << task_end << "(" << state.get_duration( changed_task ).val << ")" << std::endl;
			if ( ( state.get_latency().val < 0 || task_end > state.get_latency().val ) && state.get_duration( changed_task ).val != -1 ) {
				std::cout << "smaller than current!" << std::endl;
			    state.get_latency().set_val( task_end );
				state.get_latency().set_task( changed_task );
			}
		}

		auto before = state.get_latency().val;
		std::cout << "Lantency: Approx done. " << before << std::endl;

		if(!idl_->calc_exact(ctl)){
			undo(ctl, {});
			idl_->remove_approx(ctl.thread_id());
		    return;
		}
        state.exact = true;
		ensure_decision_level( state, level + 2 );
		for ( int changed_task : tasks_ ) {
			int task_end = idl_->get_value( ctl.thread_id(), changed_task ) + state.get_duration( changed_task ).val;
			// std::cout << task_end << std::endl;
			if ( ( state.get_latency().val < 0 || task_end > state.get_latency().val ) && state.get_duration( changed_task ).val != -1 ) {
				state.get_latency().set_val( task_end );
				state.get_latency().set_task( changed_task );
			}
		}
        auto after = state.get_latency().val;
        std::cout << "Lantency: Exact done. " << after << std::endl;
		auto accuracy = (double)before / (double)after;
		std::cout << "Accuracy: " << accuracy << std::endl;
	}

	int best_val_;
	int best_;
	bool composite = false;
	bool reason_min;
//    bool exact_;
	std::unordered_set<int> tasks_;
	std::shared_ptr<DifferenceLogicPropagator<int>> idl_;
	std::unordered_map<literal_t, std::vector<std::tuple<int, int, int>>> lit_to_val_;
	std::unordered_map<int, std::vector<std::tuple<literal_t, int, int, bool, bool, int>>> lit_to_routeval_;
	std::unordered_map<int, std::tuple<int,int,std::string,std::vector<int>>> processors; //[id] -> (x,y,name,binding)
	std::unordered_map<int, std::tuple<int,int,std::string>> comms; //[id] -> (src,dst,name)
	int width_, height_;
	std::unordered_map<int, std::string> taskIdToName_;
	std::vector<LatencyState> states_;
	std::vector<PreferenceStats> stats_;
	std::vector<int> archive_;
	std::vector<int> handles; //handle for shared memory
	std::vector<char*> memory;
	int n = 0;
	double mean_accuracy = 0.0, M2 = 0.0;
};

class LatencyCreator : public PreferenceCreator {
public:
	std::shared_ptr<Preference> create( std::string name ) {
		return std::make_shared<Latency>( Latency( name ) );
	}
};

class LatencyRegistration {
public:
	LatencyRegistration() {
		std::shared_ptr<PreferenceCreator> c;
		c.reset( new LatencyCreator() );
		PreferenceFactory::register_preference( "latency", c );
	}
};

static LatencyRegistration latencyreg;

struct SumState {
	std::vector<literal_t> trail;
	std::vector<std::pair<int, int>> stack;
	std::vector<int> sum;
};

class Sum : public Preference {
public:
	Sum( std::string name ) :
			Preference( name ) {
		best_ = -1;
	}
	void print_quality( unsigned int model ) override {
		std::cout << "Sum " << Preference::get_name() << ": " << archive_[model] << "\n";
	}
	void print_stats( int thread ) override {
		auto stat = stats_[thread];
		std::cout << "  sum " << Preference::get_name() << "\n";
		std::cout << "  total[" << thread << "]: ";
		std::cout << ( stat.undo + stat.propagate + stat.save_model + stat.better + stat.worse + stat.reason ).count() << "s\n";
		std::cout << "    init       : " << stat.init.count() << "s\n";
		std::cout << "    propagate  : " << stat.propagate.count() << "s\n";
		std::cout << "    save model : " << stat.save_model.count() << "s\n";
		std::cout << "    better     : " << stat.better.count() << "s\n";
		std::cout << "    worse      : " << stat.worse.count() << "s\n";
		std::cout << "    reason     : " << stat.reason.count() << "s\n";
		std::cout << "    undo     : " << stat.undo.count() << "s\n";

	}

	void print_stats() override {
		for ( unsigned int thread = 0; thread < stats_.size(); thread++ ) {
			print_stats( thread );
		}
	}

	void set_config( char *argv[], int argc, Control &ctl ) override {
		(void) argv;
		(void) argc;
		(void) ctl;
		return;
	}

	void save_model( unsigned int thread, unsigned int model ) override {
		Timer t { stats_[thread].save_model };
		auto &state = states_[thread];
		archive_.resize( model + 1 );
		archive_[model] = state.sum.back();
		if ( best_ == -1 || state.sum.back() < best_val_ ) {
			best_ = model;
			best_val_ = state.sum.back();
		}
	}
	bool better( unsigned int thread, unsigned int model ) override {
		Timer t { stats_[thread].better };
		if (archive_.empty()) {
			return true;
		}
		auto &state = states_[thread];
		return state.sum.back() < archive_[model];
	}

	bool bettereq( unsigned int thread, unsigned int model ) override {
		if (archive_.empty()) {
			return true;
		}
		auto &state = states_[thread];
		return state.sum.back() <= archive_[model];
	}

	bool worse( unsigned int thread, unsigned int model ) override {
		Timer t { stats_[thread].worse };
		if (archive_.empty()) {
			return false;
		}
		auto &state = states_[thread];
		return state.sum.back() > archive_[model];
	}

	std::vector<literal_t> reason( PropagateControl &ctl ) override {
		Timer t { stats_[ctl.thread_id()].reason };
        auto &state = states_[ctl.thread_id()];
		auto res = state.trail;

		for(auto lit : res){
			assert(ctl.assignment().is_true(lit));
		}
//        std::vector<literal_t> res = {};
//        int idx = state.stack.back().second;
//        state.trail.erase( state.trail.begin() + idx, state.trail.end() );
//        state.sum.pop_back();
//        state.stack.pop_back();

		return res;
	}


    bool is_empty(Clingo::id_t thread) override {
        auto& state = states_[thread];
        return state.trail.empty();
    }

	bool calc_approx(PropagateControl &ctl) override{
        __FNC__;
        // auto t1  = std::chrono::steady_clock::now();
	    auto &state = states_[ctl.thread_id()];
        auto level = ctl.assignment().decision_level();
        ensure_decision_level( state, level );
        for ( auto lit : lit_to_val_ ) {
            if(ctl.assignment().is_true(lit.first)){
                state.trail.emplace_back( lit.first );
                for ( auto cost : lit.second ) {
                    state.sum[state.sum.size() - 1] += cost;
                }
            }
        }
		// auto t2 = std::chrono::steady_clock::now();
        // std::cout << "Sum time:" << (t2-t1).count() << std::endl;
	    return true;
	}

	bool calc_exact(PropagateControl &ctl) override{
		//Sum calculation is cheap, this is why Approx = Exact
        __FNC__;
	    return true;
	}

	void remove_exact(Clingo::id_t thread) override{
        __FNC__;
	    return;
	}

	void remove_approx(Clingo::id_t thread) override{
        __FNC__;
	    auto &state = states_[thread];
	    if(state.trail.size() > 0) {
            int idx = state.stack.back().second;
            state.trail.erase(state.trail.begin() + idx, state.trail.end());
            state.sum.pop_back();
            state.stack.pop_back();
        }
        assert(state.sum.empty());
	    assert(state.trail.empty());
	    assert(state.stack.empty());
	}

	std::vector<int> current_best() override {
		return {best_};
	}

private:

	void initialize_states( PropagateInit &init ) {
		states_.resize( init.number_of_threads() );
		for ( int i = 0; i < init.number_of_threads(); ++i ) {
			states_.emplace_back();
		}
	}

	// initialization

	void init( PropagateInit &init ) override {
		stats_.resize( init.number_of_threads() );
		Timer t { stats_[0].init };
		std::vector<std::pair<std::string, int>> values;
		std::unordered_map<std::string, literal_t> symbol_table;

		for ( SymbolicAtomIterator it = init.symbolic_atoms().begin(); it != init.symbolic_atoms().end(); it++ ) {
			Symbol symbol = it->symbol();
			if ( std::strcmp( symbol.name(), "_preference" ) == 0 && symbol.arguments().size() == 5
			        && symbol.arguments()[0].to_string() == Preference::get_name() && std::strcmp( symbol.arguments()[3].name(), "name" ) != 0
			        && std::strcmp( symbol.arguments()[3].name(), "depends" ) != 0 ) {
				std::string atom = "_holds(" + symbol.arguments()[3].arguments()[0].to_string() + ",0)";
				int cost;
				if ( symbol.arguments()[4].type() == SymbolType::Number ) {
					cost = symbol.arguments()[4].number();
				} else {
					cost = symbol.arguments()[4].arguments()[0].number();
				}

				values.emplace_back( atom, cost );
			} else {
				symbol_table[symbol.to_string()] = init.solver_literal( it->literal() );
			}
		}

		for ( auto& value : values ) {
			if ( symbol_table.find( value.first ) == symbol_table.end() ) {
				std::cout << "Warning: atom " << value.first << " does not appear in symbol table\n";
			} else {
//				init.add_watch( symbol_table[value.first] );
				lit_to_val_[symbol_table[value.first]].emplace_back( value.second );
			}
		}

		initialize_states( init );
	}

	void ensure_decision_level( SumState &state, int level ) {
		if ( state.stack.size() == 0 ) {
			state.stack.emplace_back( level, state.trail.size() );
			state.sum.emplace_back( 0 );
		} else if ( state.stack.back().first < level ) {
			state.stack.emplace_back( level, state.trail.size() );
			state.sum.emplace_back( state.sum.back() );
		}
	}

	// propagation

	void propagate( PropagateControl &ctl, LiteralSpan changes ) override {
		Timer t { stats_[ctl.thread_id()].propagate };
		auto &state = states_[ctl.thread_id()];
		auto level = ctl.assignment().decision_level();
		ensure_decision_level( state, level );

		for ( auto lit : changes ) {
			auto it = lit_to_val_.find( lit );
			if ( it != lit_to_val_.end() ) {
				state.trail.emplace_back( lit );
				for ( auto cost : lit_to_val_[lit] ) {
					state.sum[state.sum.size() - 1] += cost;
				}
			}
		}
	}

	void check( PropagateControl &ctl) override {
        __FNC__;
	    auto &state = states_[ctl.thread_id()];
		auto level = ctl.assignment().decision_level();
		ensure_decision_level( state, level );
		for ( auto lit : lit_to_val_ ) {
			if(ctl.assignment().is_true(lit.first)){
				state.trail.emplace_back( lit.first );
				for ( auto cost : lit.second ) {
					state.sum[state.sum.size() - 1] += cost;
				}
			}
		}
	}

	// undo

	void undo( PropagateControl const &ctl, LiteralSpan changes ) override {
		(void) changes;
		Timer t { stats_[ctl.thread_id()].undo };
		auto &state = states_[ctl.thread_id()];
		int idx = state.stack.back().second;
		state.trail.erase( state.trail.begin() + idx, state.trail.end() );
		state.sum.pop_back();
		state.stack.pop_back();
	}

	int best_val_;
	int best_;
	bool composite = false;
	std::unordered_map<literal_t, std::vector<int>> lit_to_val_;
	std::vector<SumState> states_;
	std::vector<PreferenceStats> stats_;
	std::vector<int> archive_;
};

class SumCreator : public PreferenceCreator {
public:
	std::shared_ptr<Preference> create( std::string name ) {
		return std::make_shared<Sum>( Sum( name ) );
	}
};

class SumRegistration {
public:
	SumRegistration() {
		std::shared_ptr<PreferenceCreator> c;
		c.reset( new SumCreator() );
		PreferenceFactory::register_preference( "sum", c );
	}
};

static SumRegistration sum;

#endif /* LIBDSE_H_ */
