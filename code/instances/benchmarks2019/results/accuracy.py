import sys
import os
import glob

name=sys.argv[1]

def readfiles(err):
    
    suma=0
    counta=0

    for filename in glob.glob(name+"*mult*"+err+"*impl"):
        #print(filename)
        file = open(filename, "r")
        lines = file.readlines()
        for line in reversed(lines):
            if("Total models evaluated" in line):
                for s in line.split(): 
                    if s.isdigit():
                        counta = counta + 1
                        suma += int(s)
                break
        file.close()
    #print("Approx "+err+": " + str(suma/counta))
    approx = suma/counta

    sum=0
    count=0
    for filename in glob.glob(name+"*exact*"+err+"*impl"):
        #print(filename)
        file = open(filename, "r")
        lines = file.readlines()
        for line in reversed(lines):
            if("Total models evaluated" in line):
                for s in line.split(): 
                    if s.isdigit():
                        count = count + 1
                        sum += int(s)
                break
        file.close()
    #print("Exact "+err+":  " + str(sum/count))
    exact = sum/count
    print("Impro "+err+":  " + str(approx/float(exact)))
    
readfiles("100")
readfiles("010")
readfiles("001")