#get Pareto Front
import sys
import os
import re
import math

def getWallClockTime(line):
    match = re.search(r'[+-]?((\d+\.?\d*)|(\.\d+))\t', line)
    return float(match[0])

def getObjective(line):
    match = re.search(r'\d+$', line)
    return int(match[0])

def isDominated(vec1, vec2):
    if vec1[0] >= vec2[0] and vec1[1] >= vec2[1] and vec1[2] >= vec2[2]:
        return True
    return False

def updateFront(sol, front):
    newFront = []
    for vec in front:
        if isDominated(vec, sol):
            pass
        else:
            newFront.append(vec)
    newFront.append(sol)
    return newFront

def epsilonDominance(front, ref):
    min_ = float("inf")
    for vec_ref in ref:
        max_=0
        for vec_front in front:
            minObjective = float("inf")
            minObjective = min(vec_ref[0] / vec_front[0],vec_ref[1] / vec_front[1],vec_ref[2] / vec_front[2])
            max_ = max(max_, minObjective)
        min_=min(max_,min_)
    return min_

file = open("medium_test3_exactonly001_20200409_191756.impl")
# file = open("medium_test3_approx001_20200409_145011.impl")

# file = open("medium_test3_exactonly010_20200409_140841.impl")
# file = open("medium_test3_approx010_20200409_131426.impl")

#file = open("medium_test3_exactonly100_20200409_153307.impl")
#file = open("medium_test3_approx100_20200409_145136.impl")

paretoFront = []
solution = []
i=-1
for line in file:
    if "OPTIMUM FOUND" in line:
        wct = getWallClockTime(line)
        print("optimum found at " + str(wct) + " s.")
    if "Answer" in line:
        solution = [0,0,0]
        i=i+1
    if "Latency" in line and i > -1:
        lat = getObjective(line)
        solution[0] = lat
    if "Sum p2" in line and i > -1:
        energy = getObjective(line)
        solution[1] = energy
    if "Sum p3" in line and i > -1:
        area = getObjective(line)
        solution[2] = area
        paretoFront.append(solution)
print(i)
file.seek(0)
i = 0
fronts = {}
oldFront = []
for line in file:
    if "ANSWER FOUND" in line:
        ans = [0,0,0]
        i = i+1
        wct = getWallClockTime(line)
    if "Latency" in line:
        lat = getObjective(line)
        ans[0] = lat
    if "Sum p2" in line:
        energy = getObjective(line)
        ans[1] = energy
    if "Sum p3" in line:
        area = getObjective(line)
        ans[2] = area
        newFront = updateFront(ans, oldFront)
        fronts[wct] = newFront
        oldFront = newFront
    if "OPTIMUM FOUND" in line:
        break

if len(paretoFront) != len(newFront):
    print("Something went wrong!")
last=(0,0)
last_printed=False
for key in fronts:
    # print(key, epsilonDominance(fronts[key], paretoFront))
    epsilon= epsilonDominance(fronts[key], paretoFront)
    if epsilon!=last[1]:
        if last_printed==False:
            print("(" + str(last[0]) + "," + str(last[1])+") ", end='')
        print("(" + str(key) + "," + str(epsilon)+") ", end='')
        last_printed=True
    else:
        last_printed=False
    last=(key, epsilon)
