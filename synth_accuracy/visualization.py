"""
{{{ MIT License

Copyright 2020 Kai Neubauer <kai.neubauer at uni-rostock.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial 
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE

}}}
"""

import numpy as np
import array
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

def createSolution(dim):
    normal = rs.randint(10,100,dim)
    normal = normal.tolist()
    underapprox = [0] * dim
    for i in range(0,dim):
        value = rs.uniform(0.5,1)
        underapprox[i] = (int)(normal[i] * value)
    overapprox = [0] * dim
    for i in range(0,dim):
        value = rs.uniform(1,2)
        overapprox[i] = (int)(normal[i] * value)
    return (underapprox, normal, overapprox)

def createConvexSolution(dim, n, r, min, max):
    pop = []
    for k in range(dim):
        pop.append([])
    i=0
    while i <= n:
        vec = (max-min) * np.random.random_sample((dim,)) + min
        if np.linalg.norm(vec) >= r:
            for k in range(dim):
                pop[k].append(vec[k])
            i += 1

    return pop

def createConcaveSolution(dim, n, r, min, max):
    pop = []
    for k in range(dim):
        pop.append([])
    i=0
    while i <= n:
        vec = (max-min) * np.random.random_sample((dim,)) + min
        mysum = 0
        for k in range(dim):
            mysum += 2/(vec[k]**2)
        mysum = np.sqrt(mysum)
        if mysum <= 1/float(r):
            for k in range(dim):
                pop[k].append(vec[k])
            i += 1

    return pop

def paretoFilter(front):
    Pareto = []
    for index in range(len(front)):
        good = True
        for i in range(len(front)):
            if i == index:
                continue
            if isDominated(front[index][1], front[i][1]) <= -1:
                good = False
                break
        if good:
            Pareto.append(front[index])
    return Pareto        
    
def isDominated(vector1, vector2):
    dominated, worse, better = -2, False, False
    for i in range(0, len(vector1)):
        if vector1[i] > vector2[i]:
            worse = True
        elif vector1[i] < vector2[i]:
            better = True
    
    if worse and better:
        dominated = 0
    elif worse:
        dominated = -1
    elif better:
        dominated = 1
            
    return dominated
def jaccard(X,Y):
    A = set(map(lambda x: x[0], X))
    B = set(map(lambda x: x[0], Y))
    UNION = A | B
    INTERSECTION = A & B
    print(UNION)
    print(INTERSECTION)
    J = len(INTERSECTION) / len(UNION)
    print(J)
def isSubset(X,Y):
    for i in range(len(X)):
        isIn = False
        for j in range(len(Y)):
            if X[i][0] == Y[j][0]:
                isIn = True
                break
        if not isIn:
            return False
    return True

def check_pareto(newVector, archive):
    if len(archive)==0:
        archive.append(newVector)
    else:
        toRemove = []
        removeme = False
        # print "Check if current model is dominated by current front"
        for point in archive:
            #print point[1], point[0]
            frontVec = point
            dominated = isDominated(newVector, frontVec) 
            # check if it is dominated: -2 indifferent, -1 frontVec dominated current model, 0 incomparable, 1 current model dominates frontVec
            # print dominated
            if dominated <= -1:
                # print "new model is dominated"
                #removeme = True
                return
            #elif dominated == -1:
            #             # print "new model is dominated - break"
            #             removeme = True
            #             break
            elif dominated == 0:
                # print "new model is incomparable - continue"
                continue
            else:  # dominated > 0:
                # new model dominates current vector
                toRemove.append(point)
                continue
        for vec in toRemove:
            archive.remove(vec)
        archive.append(newVector)
    return

def check(newVector, archive):
        
    if len(archive)==0:
        return True
    else:
        removeme = False
        # print "Check if current model is dominated by current front"
        for point in archive:
#                     print point[1], point[0]
            frontVec = point
            dominated = isDominated(newVector, frontVec) 
            # check if it is dominated: -2 indifferent, -1 frontVec dominated current model, 0 incomparable, 1 current model dominates frontVec
            # print dominated
            if dominated <= -1:
                # print "new model is dominated"
                removeme = True
                break
#                     elif dominated == -1:
#                         # print "new model is dominated - break"
#                         removeme = True
#                         break
            elif dominated == 0:
#                         # print "new model is incomparable - continue"
                continue
            else:  # dominated > 0:
                # new model  still dominates frontVec. Can break
                break
        if removeme:
            return False
    return True   
np.random.seed(1000) 
dim = 2
accuracy = 0.6
approxTime = 0.5
vec = createConvexSolution(dim, 100000, 50, 10, 200)

plt.scatter(vec[0],vec[1],marker='p',color='b')
plt.axis([0,100,0,100])


candidates = []
for i in range(len(vec[0])):
    dp = []
    for k in range(dim):
        dp.append(vec[k][i])
    candidates.append(dp)
paretoFront = []
full = 0
approx = 0
full = 0
approx = 0
for vector in candidates:
    if(check([i * accuracy for i in vector], paretoFront)):
        check_pareto(vector,paretoFront)
        full += 1
    approx += 1

print "Full Eval=", full
print "Approx Eval=", approx
print "Time=", full + approx*approxTime

paretoPrint = [map(lambda x: x[0], paretoFront), map(lambda x: x[1], paretoFront)]

plt.scatter(paretoPrint[0],paretoPrint[1],marker='p',color='r')

plt.show()
data = []
for i in np.arange(0.05,1.05,0.05):
    accuracy = i
    row = []
    for j in np.arange(0.05,1.05,0.05):
        approxTime = j
        full = 0
        approx = 0
        paretoFront = []
        for vector in candidates:
            if(check([i * accuracy for i in vector], paretoFront)):
                check_pareto(vector,paretoFront)
                full += 1
            approx += 1
        time = full + approx*approxTime
        print accuracy, approxTime, time / approx
        row.append(time / approx)
    data.append(row)

fig, axis = plt.subplots() 
heatmap = axis.pcolor(data,cmap=cm.coolwarm,vmax=1.2,vmin=0.8)

data1 = np.array(data)

axis.set_yticks(np.arange(data1.shape[0])+0.5, minor=False)
axis.set_xticks(np.arange(data1.shape[1])+0.5, minor=False)

axis.set_yticklabels(list(np.arange(0.05,1.05,0.05)), minor=False)
axis.set_xticklabels(list(np.arange(0.05,1.05,0.05)), minor=False)

plt.xlabel("Time")
plt.ylabel("Accuracy")

plt.colorbar(heatmap)
plt.show()